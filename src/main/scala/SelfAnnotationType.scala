object SelfAnnotationType {
  /*
   Прилетел трейт извне, который мы никак не можем менять.
   Он инвариантен по А, т.е. если у нас А иерархичен, то
   Cheburek не "принаследует эту иерархичность"
   */
  trait Cheburek[A] {
    def eat: Unit = println("You ate me!!1!")
  }

  /*
   Мы описываем некоторую иерархию уже в нашем коде
   */
  trait Meat
  trait Pork extends Meat
  trait LiverPork extends Pork
  trait Beef extends Meat
  trait LiverBeef extends Beef

  /*
   Типы Meat и, например, LiverBeef лежат в отношении LiverBeef <: Meat
   (мы говорим, что LiverBeef является подтипом Meat).
   Но ввиду инвариантности Cheburek, Cheburek[LiverBeef] не является подтипом
   Cheburek[Meat], т.е. отношение Cheburek[LiverBeef] <: Cheburek[Meat]
   не выполняется

   P.S. понятия инвариантности, ковариантности и контрвариантности будут рассмотрены
   в следующих лекциях
   */

  trait PersonLikeMeatCheburek {self: Cheburek[Meat] => }

  /*
   Не скомпилируется, т.к. PersonLikeMeatCheburek может подмешиваться только к Cheburek[Meat]
   */
  new PersonLikeMeatCheburek with Cheburek[Pork] {}

  /*
   Используем такой трюк, чтобы обойти инвариантность типа Cheburek
   */
  trait PersonLikeMeatCheburekT[A <: Meat] {self: Cheburek[A] => }

  new PersonLikeMeatCheburekT[Pork] with Cheburek[Pork] {
    eat
  }

}
