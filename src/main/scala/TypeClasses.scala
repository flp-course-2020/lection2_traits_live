object TypeClasses {
  /*
  Требуем, чтобы тип релизовывал интерфейс Comparable,
  в итоге не можем применять эту функцию с коллекциями, элементы которых не являются Comparable
   */
  def getMax1[A <: Comparable[A]](lst: List[A]): Option[A] = lst match {
    case Nil    => None
    case x::Nil => Some(x)
    case x::xs  => getMax1(xs).map(y => if (y.compareTo(x) < 0) x else y)
  }

 /*
  Делегируем операцию сравнения отдельному трейту
 */
  trait Comparator[A] {
    def compare(x: A, y: A): Int
  }

  /*
    Получаем более гибкий код, который позволяет сравнивать не только Comparable элементы.
    Но все равно нам надо будет для всякого нового типа, который надо сравнивать, определять
    Comparator
   */
  def getMax2[A](lst: List[A], cmp: Comparator[A]): Option[A] = lst match {
    case Nil    => None
    case x::Nil => Some(x)
    case x::xs  => getMax2(xs, cmp).map(y => if (cmp.compare(y, x) > 0) x else y)
  }

  /*
   Создадим свой класс для примера
   */
  class Person(val name: String, val age: Int) {
    override def toString: String = s"$name; $age"
  }

  /*
    Т.к. Person не Comparable, определяем свой Comparator
   */
  val cmpPerson = new Comparator[Person] {
    override def compare(x: Person, y: Person): Int = y.age - x.age
  }

  val lstPerson = List(new Person("Volodya", 42), new Person("Vasily", 16))

  /*
    В дальнейшем мы увидим, что Scala позволяет организовать код так, что нам не потребуется всякий раз
    явно передавать инстанс Comparator'a и вызов функции будет иметь вид getmax2(lstPerson)
   */
  println(getMax2(lstPerson, cmpPerson))

}
