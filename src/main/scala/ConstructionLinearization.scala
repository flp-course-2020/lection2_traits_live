object ConstructionLinearization {
  trait A {
    println("A")
    def foo: String = "A"
  }

  trait B extends A{
    println("B")
    override def foo: String = "B" ++ super.foo
  }

  trait C extends B {
    println("C")
    override def foo: String = "C" ++ super.foo
  }

  trait D extends A {
    println("D")
    override def foo: String = "D" ++ super.foo
  }

  class G extends D with C with B with A

  class Ggg extends A with B with C with D

  println((new Ggg).foo)

}
